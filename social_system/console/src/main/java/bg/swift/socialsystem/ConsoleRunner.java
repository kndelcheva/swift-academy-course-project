package bg.swift.socialsystem;

import bg.swift.socialsystem.education.*;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import bg.swift.socialsystem.person.Person;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

@Service
public class ConsoleRunner implements ApplicationRunner {
    private PersonRepository personRepository;

    ConsoleRunner(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


        Scanner scanner = new Scanner(System.in);

        int numberOfPeople = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numberOfPeople; i++) {
            Person person = readNewPerson(scanner.nextLine());
            Set<Education> educations = parseEducation(scanner.nextLine());
            Set<SocialInsuranceRecord> records = readSocialInsuranceRecords(scanner.nextLine());
            person.setSocialInsuranceRecords(records);
            person.setEducations(educations);
            personRepository.save(person);
        }
    }

    private static Set<Education> parseEducation(String line) {
        String[] separatedData = line.split(";");

        Set<Education> educations = new LinkedHashSet<>();
        for (int i = 0; i < separatedData.length; i += 6) {
            char educationType = separatedData[i].charAt(0);
            String institutionName = separatedData[i + 1];
            LocalDate enrollmentDate = LocalDate.parse(separatedData[i + 2], DateTimeFormatter.ofPattern("d.M.yyyy"));
            LocalDate graduationDate = LocalDate.parse(separatedData[i + 3], DateTimeFormatter.ofPattern("d.M.yyyy"));
            boolean graduated = separatedData[i + 4].equals("YES");
            double grade = -1;
            try {
                grade = Double.parseDouble(separatedData[i + 5]);
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                // all is good
            }
            switch (educationType) {
                case 'P':
                    educations.add(new PrimaryEducation(institutionName, enrollmentDate, graduationDate, graduated));
                    break;
                case 'S':
                    if (graduated) {
                        educations.add(new SecondaryEducation(institutionName, enrollmentDate, graduationDate, graduated, grade));
                    } else {
                        educations.add(new SecondaryEducation(institutionName, enrollmentDate, graduationDate, graduated));
                    }
                    break;
                case 'B':
                    if (graduated) {
                        educations.add(new Bachelor(institutionName, enrollmentDate, graduationDate, graduated, grade));
                    } else {
                        educations.add(new Bachelor(institutionName, enrollmentDate, graduationDate, graduated));
                    }
                    break;
                case 'M':
                    if (graduated) {
                        educations.add(new Master(institutionName, enrollmentDate, graduationDate, graduated, grade));
                    } else {
                        educations.add(new Master(institutionName, enrollmentDate, graduationDate, graduated));
                    }
                    break;
                case 'D':
                    if (graduated) {
                        educations.add(new Doctorate(institutionName, enrollmentDate, graduationDate, graduated, grade));
                    } else {
                        educations.add(new Doctorate(institutionName, enrollmentDate, graduationDate, graduated));
                    }
                    break;
                default:
                    throw new RuntimeException("Invalid education type");
            }
        }
        return educations;
    }

    private static Person readNewPerson(String dataLine) {
        String[] separatedData = dataLine.split(";", 6);
        String firstName = separatedData[0];
        String lastName = separatedData[1];
        char gender = separatedData[2].charAt(0);
        int height = Integer.parseInt(separatedData[3]);
        LocalDate dateOfBirth = LocalDate.parse(separatedData[4], DateTimeFormatter.ofPattern("d.M.yyyy"));
        String address = separatedData[5];

        return new Person(firstName, lastName, gender, height, dateOfBirth, address);
    }

    private static Set<SocialInsuranceRecord> readSocialInsuranceRecords(String inputLine) {
        String[] separatedData = inputLine.split(";");
        Set<SocialInsuranceRecord> insuranceRecords = new LinkedHashSet<>();
        for (int i = 0; i < separatedData.length; i += 3) {
            double amount = Double.parseDouble(separatedData[i]);
            Year year = Year.parse(separatedData[i + 1]);
            Month month = Month.of(Integer.parseInt(separatedData[i + 2]));
            insuranceRecords.add(new SocialInsuranceRecord(amount, year, month));
        }
        return insuranceRecords;
    }
}
