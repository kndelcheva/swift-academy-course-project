package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.SecondaryEducation;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import bg.swift.socialsystem.person.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

@Controller
@RequestMapping("people")
public class PeopleController {

    private final PersonRepository repository;

    @GetMapping
    String getPeople(@RequestParam(value = "firstName", required = false) String firstName,
                     @RequestParam(value = "lastName", required = false) String lastName,
                     Model model) {
        Iterable<Person> people;

        if (firstName != null && lastName != null && !firstName.isEmpty() && !lastName.isEmpty()) {
            people = repository.findByFirstNameAndLastName(firstName, lastName);
        } else if (firstName != null && !firstName.isEmpty()) {
            people = repository.findByFirstName(firstName);
        } else if (lastName != null && !lastName.isEmpty()) {
            people = repository.findByLastName(lastName);
        } else {
            people = repository.findAll();
        }

        model.addAttribute("peopleList", people);
        return "people";
    }

    @GetMapping("{personId}")
    String getPesonDetails(@PathVariable int personId,
                           @RequestParam(value = "socialAidButtonClicked", required = false) boolean socialAidButtonClicked,
                           Model model) {
        Person person = repository.findById(personId).get();
        model.addAttribute("person", person);

        if (socialAidButtonClicked) {
            for (Education e : person.getEducations()) {
                if (e instanceof SecondaryEducation) {
                    for (SocialInsuranceRecord socialInsuranceRecord : person.getSocialInsuranceRecords()) {
                        if (socialInsuranceRecord.getDate().isAfter(YearMonth.now().minusMonths(3))) {
                            model.addAttribute("isEligible", "NOT ELIGIBLE");
                            return "personDetails";
                        }
                    }

                    double sumOfLastRecords = 0;
                    for (SocialInsuranceRecord socialInsuranceRecord : person.getSocialInsuranceRecords()) {
                        if (socialInsuranceRecord.getDate().isAfter(YearMonth.now().minusMonths(27))) {
                            sumOfLastRecords += socialInsuranceRecord.getAmount();
                        }
                    }
                    model.addAttribute("isEligible", sumOfLastRecords / 24);
                    return "personDetails";
                }
            }
            model.addAttribute("isEligible", "NOT ELIGIBLE");
        }

        return "personDetails";
    }

    @PostMapping("{personId}")
    String createInsuranceRecord(@PathVariable int personId,
                                 int year,
                                 int month,
                                 double amount,
                                 Model model) {
        SocialInsuranceRecord record = new SocialInsuranceRecord(amount, Year.of(year), Month.of(month));
        Person person = repository.findById(personId).get();
        person.addInsuranceRecord(record);
        repository.save(person);
        model.addAttribute("person", person);

        return "personDetails";
    }


    public PeopleController(PersonRepository repository) {
        this.repository = repository;
    }


}

