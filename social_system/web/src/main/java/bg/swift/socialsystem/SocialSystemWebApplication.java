package bg.swift.socialsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialSystemWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(SocialSystemWebApplication.class, args);
    }
}
