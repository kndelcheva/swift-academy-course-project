package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class PrimaryEducation extends Education {

    public PrimaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                            boolean graduated) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
    }
    public PrimaryEducation(){

    }
}
