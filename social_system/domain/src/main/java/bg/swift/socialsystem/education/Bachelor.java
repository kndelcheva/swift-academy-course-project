package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class Bachelor extends GradedEducation {

    public Bachelor(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                    boolean graduated, double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
    }

    public Bachelor(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                    boolean graduated) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
    }

    public Bachelor() {

    }


}
