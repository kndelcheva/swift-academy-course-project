package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class SecondaryEducation extends GradedEducation {

    public SecondaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                              boolean graduated, double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
    }

    public SecondaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                              boolean graduated) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
    }

    public SecondaryEducation() {

    }


}
