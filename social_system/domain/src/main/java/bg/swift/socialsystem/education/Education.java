package bg.swift.socialsystem.education;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    boolean graduated;
    private String institutionName;
    private LocalDate enrollmentDate;
    private LocalDate graduationDate;

    public Education(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, boolean graduated) {
        this.institutionName = institutionName;
        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        this.graduated = graduated;
    }

    public Education() {

    }

    public String getInstitutionName() {
        return institutionName;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(LocalDate graduationDate) {
        this.graduationDate = graduationDate;
    }

    public boolean hasGraduated() {
        return graduated;
    }

    void graduate() {
        this.graduated = true;
    }


}

