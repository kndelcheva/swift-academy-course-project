package bg.swift.socialsystem;

import bg.swift.socialsystem.person.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    List<Person> findByFirstNameAndLastName(String firstName, String lastName);

    List<Person> findByFirstName(String firstName);

    List<Person> findByLastName(String lastName);
}
