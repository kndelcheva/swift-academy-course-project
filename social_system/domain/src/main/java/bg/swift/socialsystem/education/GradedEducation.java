package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public abstract class GradedEducation extends Education {
    private double finalGrade = -1;


    public GradedEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                           boolean graduated, double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
        this.finalGrade = finalGrade;
    }

    public GradedEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                           boolean graduated) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
    }

    public GradedEducation() {

    }

    public double getFinalGrade() {
        return finalGrade;
    }


}

